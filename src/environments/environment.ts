// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const API_PROTOCOL = window.location.protocol;
const API_HOSTNAME = window.location.hostname;
const API_PORT = 3000;

export const environment = {
  production: false,
  API_HOST : `${API_PROTOCOL}//${API_HOSTNAME}:${API_PORT}`,
  STATIC_HOST : `http://localhost:3000`,
  VER : '0.0.1'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

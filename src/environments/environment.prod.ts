const API_PROTOCOL = window.location.protocol;
const API_HOSTNAME = window.location.hostname;
const API_PORT = window.location.port;

export const environment = {
  production: true,
  API_HOST : `${API_PROTOCOL}//${API_HOSTNAME}:${API_PORT}`,
  STATIC_HOST : `${API_PROTOCOL}//${API_HOSTNAME}:${API_PORT}`,
  VER : '0.0.1'
};

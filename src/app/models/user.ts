export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    avatar: string;
    gender: string;
    token: string;
}
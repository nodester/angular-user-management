import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralComponent, MasterComponent } from '@app/layout';
import { LoginComponent } from '@app/components';
import { RedirectComponent } from './components/redirect/redirect.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: '',
    component: GeneralComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
        data: { title: 'Login', page: 'login' }
      },
      {
        path: 'redirect',
        component: RedirectComponent,
        data: { title: 'User Management', page: 'redirect' }
      },
    ]
  },
  {
    path: '',
    component: MasterComponent,
    children: [
      {
        path: 'user',
        loadChildren: './modules/user/user.module#UserModule'
      },
      {
        path: 'admin',
        loadChildren: './modules/admin/admin.module#AdminModule'
      },
    ]
  },
  { path: '**', redirectTo: 'redirect' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { startWith } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AjaxService {

  constructor(private httpClient: HttpClient) { }

  get(config): Observable<any> {

    const params = new HttpParams();
    for (const param of config.params) {
      params.append(param, config.params[param]);
    }

    const headers = new HttpHeaders({
      Accept: 'text/html; charset=utf-8'
    });

    const url = config.url;
    let response = this.httpClient.get(url, { params, headers, withCredentials: false });

    if (config.cacheKey) {
      response.subscribe(next => {
        localStorage[config.cacheKey] = JSON.stringify(next);
      });

      response = response.pipe(
        startWith(JSON.parse(localStorage[config.cacheKey] || '[]'))
      );
    }


    return response;
  }

  post(config): Observable<any> {
    const httpOptions = {
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    const url = config.url;
    const response = this.httpClient.post(url, config.data, httpOptions);
    return response;
  }

  put(config): Observable<any> {
    const httpOptions = {
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    const url = config.url;
    const response = this.httpClient.post(url, config.data, httpOptions);
    return response;
  }

  delete(config): Observable<any> {
    const httpOptions = {
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    const url = config.url;
    const response = this.httpClient.post(url, config.data, httpOptions);
    return response;
  }
}

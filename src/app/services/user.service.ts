import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '@app/models';
import { AjaxService } from './ajx.service';

@Injectable({ providedIn: 'root' })
export class UserService {
    user: User;
    constructor(private ajaxService: AjaxService) { }

    get User(): User {
        return this.user;
    }

    set User(u: User) {
        this.user = u;
    }

    getAll() {
        const config = {};
        return this.ajaxService.get(config);
    }

    getById(id: number) {
        const config = {};
        return this.ajaxService.get(config);
    }

    register(user: User) {
        const config = {};
        return this.ajaxService.post(config);
    }

    update(user: User) {
        const config = {};
        return this.ajaxService.put(config);
    }

    delete(id: number) {
        const config = {};
        return this.ajaxService.delete(config);
    }
}

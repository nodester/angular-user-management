import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, pipe } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '@app/models';
import { AjaxService } from './ajx.service';
import { ApiService } from './api.service';
import { UserService } from './user.service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    // private currentUserTokenSubject: BehaviorSubject<string>;
    // public currentUserToken: Observable<string>;

    constructor(
        private ajaxService: AjaxService,
        private apiService: ApiService,
        private userService: UserService
    ) {
        // this.currentUserTokenSubject = new BehaviorSubject<string>(localStorage.getItem('token'));
        // this.currentUserToken = this.currentUserTokenSubject.asObservable();
    }

    public get userToken(): string {
        return localStorage.getItem('token');
    }

    authenticate() {
        const { API_CONFIG, API_URLs } = this.apiService;

        const config = {
            url: `${API_CONFIG.API_HOST}${API_URLs.USERS.current}`,
            params: {},
            cacheKey: false
        };

        return new Observable(observer => {

            this.ajaxService.get(config)
                .subscribe((result) => {
                    console.log(result);
                    const user = result;
                    if (user && user.token) {
                        // user is legit
                        // this.currentUserTokenSubject.next(user);
                    }
                    this.userService.User = user;
                    observer.next(user);
                    observer.complete();
                }, (error) => {
                    observer.next(false);
                    observer.complete();
                }, () => {
                    observer.next();
                    observer.complete();
                    console.log('The observable is now completed.');
                });

        });
    }

    login(username: string, password: string) {

        const { API_CONFIG, API_URLs } = this.apiService;

        const config = {
            url: `${API_CONFIG.API_HOST}${API_URLs.USERS.authenticate}`,
            data: { username, password },
            cacheKey: false
        };

        return new Observable(observer => {

            this.ajaxService.post(config)
                .subscribe((result) => {
                    console.log(result);
                    const user = result;
                    if (user && user.token) {
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        // localStorage.setItem('currentUserToken', JSON.stringify(user));
                        localStorage.setItem('token', user.token);
                        // this.currentUserTokenSubject.next(user);
                    }
                    this.userService.User = user;
                    observer.next(user);
                    observer.complete();
                }, (error) => {
                    observer.next({ error });
                    observer.complete();
                }, () => {
                    observer.next();
                    observer.complete();
                    console.log('The observable is now completed.');
                });

        });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('token');
        // this.currentUserTokenSubject.next(null);
    }
}
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

const API_CONFIG = {
    API_HOST: environment.API_HOST,
};

const api = '/api';
const ver = '/v1';
const userAPIUrl = `${api}${ver}/users`;

/**
 * @const {object} API
 * @desc List of all API urls
 */
const API_URLS = {
    USERS : {
        allUsers : `${userAPIUrl}/`,
        authenticate: `${userAPIUrl}/authenticate`,
        register : `${userAPIUrl}/register`,
        current : `${userAPIUrl}/current`,
        userById : `${userAPIUrl}/`, // Param : ID
        update : `${userAPIUrl}/`, // Param : ID
        delete : `${userAPIUrl}/`, // Param : ID
    }
};

@Injectable({
        providedIn: 'root'
    })
export class ApiService {

    constructor() { }

    get API_URLs() {
        return API_URLS;
    }

    get API_CONFIG() {
        return API_CONFIG;
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertComponent, LoginComponent, RegisterComponent, HomeComponent } from '@app/components';
import { RouterModule } from '@angular/router';
import { RedirectComponent } from '@app/components/redirect/redirect.component';

@NgModule({
  declarations: [
    AlertComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    RedirectComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AlertComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    RedirectComponent
  ]
})
export class SharedModule { }

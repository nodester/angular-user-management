
import { Role } from '@app/models';
const redirectURL = {};

redirectURL[Role.Admin] = '/admin/home';
redirectURL[Role.User] = '/user/home';

export { redirectURL };

import { Component, OnInit } from '@angular/core';
import { AuthenticationService, UserService } from '@app/services';
import { Router, ActivatedRoute } from '@angular/router';
import { redirectURL } from '@app/helpers';
const returnUrl = 'returnUrl';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.sass']
})
export class RedirectComponent implements OnInit {

  returnUrl: string;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    // console.log('redirect', this.authenticationService.userToken);

    if (this.authenticationService.userToken) {
      this.authenticationService.authenticate()
        .subscribe((data: any) => {

          if (data.error) {
            this.authenticationService.logout();
            this.router.navigate(['/login']);
          }

          const defaultRedirectUrl = this.route.snapshot.queryParams[returnUrl] || redirectURL[data.role];
          console.log(defaultRedirectUrl);
          this.router.navigate([defaultRedirectUrl]);
        },
          error => {
            // console.log(error);
            this.authenticationService.logout();
            this.router.navigate(['/login']);
          });
    } else {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    // console.log('Redirect');
  }

}

import { Component, OnInit } from '@angular/core';
import { UserService, AuthenticationService } from '@app/services';
import { User } from '@app/models';
import { Router } from '@angular/router';
import { environment } from '@env/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  user: User;
  avatar: String;
  constructor(
    private userService: UserService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.user = this.userService.User;
    this.avatar = `${environment.STATIC_HOST}/${this.user.username}/${this.user.avatar}`
  }

  logout(){
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
